#include "main.h"

auto main() -> int
{
    rddb::Client client;
    if (client.initialize())
    {
        return static_cast<int>(client.run());
    }
    else
    {
        std::cerr << "Should not be possible\n";
        return 1;
    }
}