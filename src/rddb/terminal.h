#ifndef RDDB_TERMINAL
#define RDDB_TERMINAL

#include <iostream>
#include <mutex>
#include <string_view>
#include <sstream>
#include <thread>
#include <unistd.h>

#include "ascii_art.h"
#include "commands.h"
#include "window.h"

namespace rddb
{
    enum struct CommandType
    {
        Unknown,
        Continue,
        Repeat,
        Exit,
    };

    enum struct InteractionResult
    {
        Exit,
        Continue,
        Failure,
        Uninitialized,
    };

    enum struct TerminalState
    {
        Subject,
        Problem,
        Specialization,
    };

    class TerminalResult
    {
        public:
            TerminalResult() : TerminalResult("", CommandType::Unknown) {}
            TerminalResult(std::string_view matched_command, CommandType command_type)
            {
                this->matched_command = matched_command;
                this->command_type = command_type;
            }
            TerminalResult(const TerminalResult&) = delete;
            TerminalResult& operator= (const TerminalResult& other)
            {
                this->matched_command = other.matched_command;
                this->command_type = other.command_type;
                return *this;
            }

            ~TerminalResult() = default;

            CommandType command_type;
            std::string_view matched_command;
            std::string_view message;
    };

    class Terminal
    {
        public:
            bool should_exit;
            
            Terminal()
            {
                this->current_duck_frame = 0;
                this->initialized = false;
                this->should_exit = false;
                this->terminal_state = TerminalState::Subject;
            }
            Terminal(const Terminal&) = delete;
            Terminal& operator= (const Terminal&) = delete;

            ~Terminal() = default;

            auto constexpr exit() -> void
            {
                this->should_exit = true;
            }

            auto initialize() -> bool
            {
                if (
                    this->input_window.initialize(
                        INPUT_WIDTH,
                        INPUT_HEIGHT,
                        INPUT_X,
                        INPUT_Y
                    )
                    && this->output_window.initialize(
                        OUTPUT_WIDTH,
                        OUTPUT_HEIGHT,
                        OUTPUT_X,
                        OUTPUT_Y
                    )
                )
                {
                    this->initialized = true;
                }
                this->output_thread = std::thread(&Terminal::hello, this);
                return this->is_initialized();
            }

            auto is_initialized() -> bool
            {
                return this->initialized;
            }

            auto interact() -> InteractionResult
            {
                if (!this->is_initialized())
                {
                    return InteractionResult::Uninitialized;
                }
                TerminalResult result;
                switch (this->terminal_state)
                {
                    case TerminalState::Subject:
                    {
                        this->printing_mutex.lock();
                        this->input_window.print("What do you need help with today?\n", 0, 0);
                        this->input_window.blit();
                        this->printing_mutex.unlock();
                        auto line = this->input_window.get_line();
                        result = this->match_subject_command(line);
                        switch (result.command_type)
                        {
                            case CommandType::Continue:
                            {
                                this->terminal_state = TerminalState::Problem;
                                
                                std::stringstream stream;
                                stream << "Okay! What about '" << result.matched_command << "' do you need help with?\n";
                                
                                this->printing_mutex.lock();
                                this->input_window.clear();
                                this->input_window.print(stream.str());
                                this->input_window.blit();
                                this->printing_mutex.unlock();
                                break;
                            }
                            case CommandType::Repeat:
                            {
                                this->terminal_state = TerminalState::Subject;
                                
                                this->printing_mutex.lock();
                                this->input_window.clear();
                                this->input_window.print("Sorry, I don't know about that specific thing.", OUTPUT_X_OFFSET, OUTPUT_Y_OFFSET);
                                this->input_window.blit();
                                this->printing_mutex.unlock();
                                break;
                            }
                            case CommandType::Exit:
                            {
                                this->should_exit = true;
                                break;
                            }
                            case CommandType::Unknown:
                            {
                                this->should_exit = true;
                                break;
                            }
                        }
                        this->last_result = result;
                        break;
                    }
                    case TerminalState::Problem:
                    {
                        auto line = this->input_window.get_line();
                        result = this->match_problem_command(line);
                        switch (result.command_type)
                        {
                            case CommandType::Continue:
                            {
                                this->terminal_state = TerminalState::Specialization;
                                
                                this->printing_mutex.lock();
                                this->input_window.clear();
                                this->input_window.print("You need a special answer about what?\n");
                                this->input_window.blit();
                                this->printing_mutex.unlock();
                                break;
                            }
                            case CommandType::Repeat:
                            {
                                this->terminal_state = TerminalState::Subject;
                                
                                this->printing_mutex.lock();
                                this->input_window.clear();
                                this->input_window.print("Sorry, I don't know about that specific thing.", OUTPUT_X_OFFSET, OUTPUT_Y_OFFSET);
                                this->input_window.blit();
                                this->printing_mutex.unlock();
                                break;
                            }
                            case CommandType::Exit:
                            {
                                this->should_exit = true;
                                break;
                            }
                            case CommandType::Unknown:
                            {
                                this->should_exit = true;
                                break;
                            }
                        }
                        this->last_result = result;
                        break;
                    }
                    case TerminalState::Specialization:
                    {
                        auto line = this->input_window.get_line();
                        result = this->match_specialization_command(line);
                        switch (result.command_type)
                        {
                            case CommandType::Continue:
                            {
                                this->terminal_state = TerminalState::Subject;
                                
                                this->printing_mutex.lock();
                                this->input_window.clear();
                                this->input_window.print("Some special answer.", OUTPUT_X_OFFSET, OUTPUT_Y_OFFSET);
                                this->input_window.blit();
                                this->printing_mutex.unlock();
                                break;
                            }
                            case CommandType::Repeat:
                            {
                                this->terminal_state = TerminalState::Subject;
                                
                                this->printing_mutex.lock();
                                this->input_window.clear();
                                this->input_window.print("Sorry, I don't know anything special about that thing.", OUTPUT_X_OFFSET, OUTPUT_Y_OFFSET);
                                this->input_window.blit();
                                this->printing_mutex.unlock();
                                break;
                            }
                            case CommandType::Exit:
                            {
                                this->should_exit = true;
                                break;
                            }
                            case CommandType::Unknown:
                            {
                                this->should_exit = true;
                                break;
                            }
                        }
                        this->last_result = result;
                        break;
                    }
                }

                return this->should_exit ? InteractionResult::Exit : InteractionResult::Continue;
            }

        private:
            static const int SLEEP_DURATION = 1;

            static const int INPUT_WIDTH = 80;
            static const int INPUT_HEIGHT = 5;
            static const int INPUT_X = 0;
            static const int INPUT_Y = 23;

            static const int OUTPUT_WIDTH = 80;
            static const int OUTPUT_HEIGHT = 22;
            static const int OUTPUT_X = 0;
            static const int OUTPUT_X_OFFSET = 0;
            static const int OUTPUT_Y = 0;
            static const int OUTPUT_Y_OFFSET = 3;
            
            int current_duck_frame;
            std::thread output_thread;
            bool initialized;
            Window input_window;
            std::mutex printing_mutex;
            Window output_window;
            TerminalState terminal_state;
            TerminalResult last_result;

            auto hello() -> void
            {
                while (!this->should_exit)
                {
                    int x = 0;
                    int y = 0;
                    this->printing_mutex.lock();
                    this->input_window.get_cursor_position(x, y);
                    
                    this->output_window.clear();
                    this->output_window.print("Welcome to RDDB! The Rubber Ducky Debugger.", 0, 0);
                    this->output_window.print(duck_frames[this->current_duck_frame % duck_frames.size()], 0, 1);
                    this->output_window.blit();
                    this->current_duck_frame++;

                    this->input_window.set_cursor_position(x, y);
                    this->printing_mutex.unlock();
                    
                    sleep(SLEEP_DURATION);
                }
            }

            auto match_problem_command(std::string_view command) -> TerminalResult
            {
                for (auto exit_command : commands::exit_commands)
                {
                    if (command.find(exit_command) != std::string_view::npos)
                    {
                        return TerminalResult{
                            exit_command,
                            CommandType::Exit
                        };
                    }
                }
                for (auto continue_command : commands::continue_commands)
                {
                    if (command.find(continue_command) != std::string::npos)
                    {
                        return TerminalResult{
                            continue_command,
                            CommandType::Continue
                        };
                    }
                }

                return TerminalResult {
                    "",
                    CommandType::Repeat
                };
            }

            auto match_specialization_command(std::string_view command) -> TerminalResult
            {
                for (auto exit_command : commands::exit_commands)
                {
                    if (command.find(exit_command) != std::string_view::npos)
                    {
                        return TerminalResult{
                            exit_command,
                            CommandType::Exit
                        };
                    }
                }
                for (auto continue_command : commands::continue_commands)
                {
                    if (command.find(continue_command) != std::string::npos)
                    {
                        return TerminalResult{
                            continue_command,
                            CommandType::Continue
                        };
                    }
                }

                return TerminalResult {
                    "",
                    CommandType::Repeat
                };
            }

            auto match_subject_command(std::string_view command) -> TerminalResult
            {
                for (auto exit_command : commands::exit_commands)
                {
                    if (command.find(exit_command) != std::string_view::npos)
                    {
                        return TerminalResult{
                            exit_command,
                            CommandType::Exit
                        };
                    }
                }
                for (auto continue_command : commands::continue_commands)
                {
                    if (command.find(continue_command) != std::string::npos)
                    {
                        return TerminalResult{
                            continue_command,
                            CommandType::Continue
                        };
                    }
                }

                return TerminalResult {
                    "",
                    CommandType::Repeat
                };
            }
    };
}

#endif
