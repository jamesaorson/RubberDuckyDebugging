#ifndef RDDB_COMMANDS
#define RDDB_COMMANDS

#include <array>

namespace rddb
{
    namespace commands
    {
        static const std::array<const char*, 4> exit_commands = {
            "die",
            "kill",
            "quit",
            "exit"
        };
        
        static const char* NOTHING = "nothing";
        static const std::array<const char*, 1> continue_commands = {
            NOTHING
        };
    }
}

#endif
