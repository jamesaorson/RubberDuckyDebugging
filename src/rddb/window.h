#ifndef RDDB_WINDOW
#define RDDB_WINDOW

#include <ncurses.h>
#include <string_view>
#include <vector>

namespace rddb
{
    class Window
    {
        public:
            Window()
            {
                this->initialized = false;
                this->window = nullptr;
            }
            Window(const Window&) = delete;
            Window& operator= (const Window&) = delete;

            ~Window()
            {
                if (this->is_initialized())
                {
                    delwin(this->window);
                    endwin();
                }
            }

            auto blit() -> bool
            {
                if (!this->is_initialized())
                {
                    return false;
                }

                wrefresh(this->window);
                return true;
            }

            auto clear() -> bool
            {
                if (!this->is_initialized())
                {
                    return false;
                }
                werase(this->window);
                return true;
            }

            auto get_line() -> std::string_view
            {
                if (!this->is_initialized())
                {
                    return "";
                }
                this->line.clear();
                
                auto character = static_cast<char>(wgetch(this->window));
                while (character != '\r' && character != '\n')
                {
                    int x;
                    int y;
                    if (!this->get_cursor_position(x, y))
                    {
                        return "";
                    }
                    // std::cout << (unsigned int)character << ' ' << KEY_BACKSPACE;
                    switch (character)
                    {
                        case ERR:
                        case KEY_UP:
                        case KEY_DOWN:
                        case KEY_LEFT:
                        case KEY_RIGHT:
                        {
                            break;
                        }
                        case 127: // Mac DELETE key
                        case KEY_BACKSPACE:
                        case KEY_DL:
                        case KEY_DC:
                        {
                            if (this->line.empty())
                            {
                                break;
                            }
                            this->print(' ', x - 1, y, true);
                            this->set_cursor_position(x - 1, y);
                            this->line.pop_back();
                            break;
                        }
                        default:
                        {
                            this->line.push_back(character);
                            this->print(character, x, y, true);
                            break;
                        }
                    }
                    character = static_cast<char>(wgetch(this->window));
                }
                return std::string_view(this->line.data());
            }

            auto initialize(int width, int height, int x = 0, int y = 0) -> bool
            {
                initscr();
                cbreak();
                noecho();
                this->window = newwin(height, width, y, x);
                keypad(this->window, true);
                nodelay(this->window, true);

                this->initialized = true;
                return this->is_initialized();
            }

            auto inline is_initialized() -> bool
            {
                return this->initialized;
            }

            auto print(std::string_view message, int x = 0, int y = 0, bool auto_blit = false) -> bool
            {
                if (!this->is_initialized())
                {
                    return false;
                }
                mvwprintw(this->window, y, x, message.data());
                wrefresh(this->window);
                if (auto_blit)
                {
                    this->blit();
                }

                return true;
            }

            auto print(char character, int x = 0, int y = 0, bool auto_blit = false) -> bool
            {
                if (!this->is_initialized())
                {
                    return false;
                }
                mvwaddch(this->window, y, x, character);
                wrefresh(this->window);
                if (auto_blit)
                {
                    this->blit();
                }

                return true;
            }

            auto get_cursor_position(int& x, int& y) -> bool
            {
                if (!this->is_initialized())
                {
                    return false;
                }
                x = getcurx(this->window);
                y = getcury(this->window);

                return true;
            }

            auto set_cursor_position(int x, int y) -> bool
            {
                if (
                    !this->is_initialized()
                    || x < 0 || x >= COLS
                    || y < 0 || y >= LINES
                )
                {
                    return false;
                }

                wmove(this->window, y, x);
                return true;
            }
        
        private:
            bool initialized;
            std::vector<char> line;
            WINDOW* window;
    };
}

#endif
